#include "AIEngine.hpp"
#include "TrtInfer.hpp"

AIEngine::AIEngine()
{
	m_processSys = new TrtInfer();
}

AIEngine::~AIEngine()
{
	if (m_processSys != NULL)
	{
		delete (TrtInfer*)m_processSys;
		m_processSys = NULL;
	}
}

bool AIEngine::Init(const InitParam& initParam)
{
	return ((TrtInfer*)m_processSys)->Init(initParam);
}

void AIEngine::Uninit()
{
	((TrtInfer*)m_processSys)->Uninit();
}

bool AIEngine::Process(std::vector<float>& inputData, std::vector<float>& outputData)
{
	return ((TrtInfer*)m_processSys)->Process(inputData, outputData);
}