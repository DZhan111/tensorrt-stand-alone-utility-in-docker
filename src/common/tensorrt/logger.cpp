#include "logger.h"
#include "ErrorRecorder.h"
#include "logging.h"

SampleErrorRecorder gRecorder;

Logger gLogger{Logger::Severity::kINFO};
LogStreamConsumer gLogVerbose{TRT_LOG_VERBOSE(gLogger)};
LogStreamConsumer gLogInfo{TRT_LOG_INFO(gLogger)};
LogStreamConsumer gLogWarning{TRT_LOG_WARN(gLogger)};
LogStreamConsumer gLogError{TRT_LOG_ERROR(gLogger)};
LogStreamConsumer gLogFatal{TRT_LOG_FATAL(gLogger)};

void setReportableSeverity(Logger::Severity severity)
{
    gLogger.setReportableSeverity(severity);
    gLogVerbose.setReportableSeverity(severity);
    gLogInfo.setReportableSeverity(severity);
    gLogWarning.setReportableSeverity(severity);
    gLogError.setReportableSeverity(severity);
    gLogFatal.setReportableSeverity(severity);
}
