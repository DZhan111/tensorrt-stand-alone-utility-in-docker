cmake_minimum_required(VERSION 3.12.0)

set(lib_name AIEngine)

project(${lib_name})


#############################  source files #############################
set(CMAKE_SOURCE_LIST
	cmake/source.cmake
	cmake/incLink.cmake)
source_group(TREE ${CMAKE_CURRENT_SOURCE_DIR}/../ FILES ${CMAKE_SOURCE_LIST})

include(cmake/source.cmake)

#############################  add_library #############################  
cuda_add_library(${lib_name} 
				${LINK_TYPE} 
				${Common_SOURCE_LIST} 
				${SOURCE_LIST} 
				${CMAKE_SOURCE_LIST})

#############################  dependencies #############################  
include(cmake/incLink.cmake)

target_include_directories(${lib_name} PUBLIC $<INSTALL_INTERFACE:include>)
target_include_directories(${lib_name} PRIVATE ${CMAKE_SOURCE_DIR}/common)

set_target_properties(${lib_name} PROPERTIES CXX_STANDARD 17)



