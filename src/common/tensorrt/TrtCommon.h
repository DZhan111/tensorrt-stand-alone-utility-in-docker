#pragma once

#include<string>
#include<vector>
#include <NvInfer.h>
#include <memory>

using namespace nvinfer1;

struct BuildParams
{
    std::string trtFileName;
    std::string onnxFile;
    char* onnxModel;
    uint32_t onnxModelSize;
    int32_t batchSize{ 1 };              //!< Number of inputs in a batch
    int32_t dlaCore{ -1 };               //!< Specify the DLA core to run network on.
    bool int8{ false };                  //!< Allow runnning the network in Int8 mode.
    bool fp16{ false };                  //!< Allow running the network in FP16 mode.
    std::vector<std::string> dataDirs;   //!< Directory paths where sample data files are stored
    std::vector<std::string> inputTensorNames;
    std::vector<std::string> outputTensorNames;
    std::vector<nvinfer1::Dims4> minSize;
    std::vector<nvinfer1::Dims4> maxSize;
    std::vector<nvinfer1::Dims4> optSize;
    bool isEncrypted = false;
};

bool Onnx2Trt(const BuildParams& params, std::shared_ptr<ICudaEngine>& engine, std::shared_ptr<IExecutionContext>& context);

bool LoadTrtEngine(const std::string trtFileName, std::shared_ptr<ICudaEngine>& engine, std::shared_ptr<IExecutionContext>& context);