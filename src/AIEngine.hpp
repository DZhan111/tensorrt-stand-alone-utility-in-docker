
#pragma once

#if (defined _WIN32 || defined WIN32) && defined PI_API_EXPORTS
#  define API_EXPORTS __declspec(dllexport)
#elif defined __GNUC__ && __GNUC__ >= 4
#  define API_EXPORTS __attribute__ ((visibility ("default")))
#else
#  define API_EXPORTS __declspec(dllimport)
#endif

#include <vector>
#include <string>

struct InitParam
{
	/// model param
	std::string modelPath = "";

	/// tensorrt param
	std::string trtModelPath = "";
};

class API_EXPORTS AIEngine
{
public:
	AIEngine();

	~AIEngine();

	/// <summary>
	/// @brief Init for AIEngine
	/// </summary>
	bool Init(const InitParam& initParam);

	/// <summary>
	/// @brief Uninit for AIEngine
	/// </summary>
	void Uninit();

	/// <summary>
	/// @brief do inference for AIEngine
	/// </summary>
	/// <param name="inputData"> input, Original image to be processed </param>
	/// <param name="outputData"> output </param>
	bool Process(std::vector<float>& inputData, std::vector<float>& outputData);

private:
	void * m_processSys;
};

