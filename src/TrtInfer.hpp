#pragma once

#include "AIEngine.hpp"
#include "buffers.h"
#include <NvInfer.h>
#include <vector>

class TrtInfer
{
public:
	TrtInfer();
	~TrtInfer();

	bool Init(const InitParam& initParam);

	void Uninit();

	bool Process(std::vector<float>& inputData, std::vector<float>& outputData);

private:
	void GetInputInfos();
	void GetOutputInfos();

	bool TrtProcess(std::vector<float>& inpuData);

private:
	std::string m_engineName = "trt.engine";

	int m_maxBatchSize;

	BufferManager* m_buffers;

	std::vector<size_t> m_inputSize;
	std::vector<std::string> m_inputNames;
	std::vector<std::vector<float>> m_inputData;
	std::vector<float*> m_inputHostDataBuffer;
	std::vector<float*> m_inputDeviceDataBuffer;

	std::vector<size_t> m_outputSize;
	std::vector<std::string> m_outputNames;
	std::vector<float*> m_outputHostDataBuffer;
	std::vector<float*> m_outputDeviceDataBuffer;

	cudaStream_t m_stream;
	std::shared_ptr<nvinfer1::ICudaEngine> m_engine;
	std::shared_ptr<nvinfer1::IExecutionContext> m_context;

private:

    bool PostProcess(std::vector<float>& outputData);

private:
    std::vector<size_t> m_inputDataSize;
    std::vector<std::vector<int64_t>> m_inputShape;

    std::vector<size_t> m_outputDataSize;
    std::vector<std::vector<int64_t>> m_outputShape;

    float* m_predData;
    std::vector<int64_t> m_predShape;
};