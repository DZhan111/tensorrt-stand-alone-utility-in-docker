#include "Utils.hpp"
#ifdef _WIN32
#include <Codecvt>
#endif
#include <filesystem>
#include <iostream>
#include <string>
#include <fstream>

namespace bfs = std::filesystem;


size_t readBinaryFile(const char* filename, unsigned char*& data)
{
	FILE* fp;
#ifdef _WIN32
	fopen_s(&fp, filename, "rb");
#else
	fp = fopen(filename, "rb");
#endif
	if (fp == NULL) return 0; 
	fseek(fp, 0, SEEK_END);  
	size_t size = ftell(fp); 
	fseek(fp, 0, SEEK_SET); 
	data = (unsigned char*)malloc(sizeof(unsigned char) * size);
	fread(data, size, 1, fp);
	fclose(fp);
	return size;
}

size_t writeBinaryFile(const char* filename, const unsigned char* data, size_t size)
{
	FILE* fp;
#ifdef _WIN32
	fopen_s(&fp, filename, "wb");
#else
	fp = fopen(filename, "wb");
#endif
	if (fp == NULL) return 0;
	size_t written_size = fwrite(data, size, 1, fp);
	fclose(fp);
	return written_size;
}

bool is_str_utf8(const char* str)
{
	unsigned int nBytes = 0;
	unsigned char chr = *str;
	bool bAllAscii = true;
	for (unsigned int i = 0; str[i] != '\0'; ++i) {
		chr = *(str + i);
		if (nBytes == 0 && (chr & 0x80) != 0) {
			bAllAscii = false;
		}
		if (nBytes == 0) {
			if (chr >= 0x80) {

				if (chr >= 0xFC && chr <= 0xFD) {
					nBytes = 6;
				}
				else if (chr >= 0xF8) {
					nBytes = 5;
				}
				else if (chr >= 0xF0) {
					nBytes = 4;
				}
				else if (chr >= 0xE0) {
					nBytes = 3;
				}
				else if (chr >= 0xC0) {
					nBytes = 2;
				}
				else {
					return false;
				}

				nBytes--;
			}
		}
		else {
			if ((chr & 0xC0) != 0x80) {
				return false;
			}
			nBytes--;
		}
	}
	if (nBytes != 0) {
		return false;
	}
	if (bAllAscii) {
		return true;
	}
	return true;
}

std::wstring UTF8ToUnicode(const std::string& str)
{
	std::wstring ret;
	std::wstring_convert< std::codecvt_utf8<wchar_t> > wcv;
	ret = wcv.from_bytes(str);
	return ret;
}

std::filesystem::path getBoostPath(const std::string& strPath) {
	bfs::path boostPath;
	if (is_str_utf8(strPath.c_str())) {
		std::wstring wpath = UTF8ToUnicode(strPath);
		boostPath = bfs::path(wpath);
	}
	else {
		boostPath = bfs::path(strPath);
	}
	return boostPath;
}

bool isExistFile(const std::string& path) {
	bool ret = false;
	const int PATH_MAX_local = 4096;
	if (path.length() > PATH_MAX_local) {
		ret = false;
	}
	else {
		bfs::path root = getBoostPath(path);
		if (!bfs::exists(root) || !bfs::is_regular_file(root)) {
			ret = false;
		}
		else {
			ret = true;
		}
	}
	return ret;
}

std::string UnicodeToUTF8(const std::wstring& wstr)
{
	std::string ret;
	std::wstring_convert< std::codecvt_utf8<wchar_t> > wcv;
	ret = wcv.to_bytes(wstr);
	return ret;
}

std::string contactPath(const std::string& path1, const std::string& path2)
{
	bfs::path p1 = getBoostPath(path1);
	bfs::path p2 = getBoostPath(path2);
	std::wstring wpath = (p1 / p2).wstring();
	std::string path = UnicodeToUTF8(wpath);
	return path;
}

bool isExistDirectory(const std::string& directory) {
	const int PATH_MAX_local = 4096;
	if (directory.length() > PATH_MAX_local) {
		return false;
	}
	bfs::path root = getBoostPath(directory);
	if (!bfs::exists(root) || !bfs::is_directory(root)) {
		return false;
	}
	else {
		return true;
	}
}

bool createDiretory(const std::string& directory) {
	if (!isExistDirectory(directory)) {
		bfs::path root = getBoostPath(directory);
		return bfs::create_directories(root);
	}
	return true;
}