
#include "TrtCommon.h"
#include "common.h"
#include "Utils.hpp"
#include <iostream>
#include <fstream>
#include <sstream>
#include <NvOnnxParser.h>
#include <NvInferPlugin.h>

using trtCommon::TrtUniquePtr;

bool ConstructNetwork(const BuildParams& params, TrtUniquePtr<nvinfer1::IBuilder>& builder,
						TrtUniquePtr<nvinfer1::INetworkDefinition>& network, TrtUniquePtr<nvinfer1::IBuilderConfig>& config,
						TrtUniquePtr<nvonnxparser::IParser>& parser)
{
	bool parsed = false;
	if (params.isEncrypted)
	{
		parsed = parser->parse(params.onnxModel, params.onnxModelSize);
	}
	else
	{
		if (!isExistFile(params.onnxFile))
		{
			// PLOGE << "OnnxPath does not exists: " << params.onnxFile;
			return false;
		}

		parsed = parser->parseFromFile(params.onnxFile.c_str(), static_cast<int>(gLogger.getReportableSeverity()));
	}

	if (!parsed)
	{
		// PLOGE << "parseFromFile failed: " << params.onnxFile;
		return false;
	}

	config->setMaxWorkspaceSize(4_GiB);

	if (builder->platformHasFastFp16())
	{
		config->setFlag(BuilderFlag::kFP16);
		std::cout<<"!!!!!!!!!!!!!!!TTTTTTTTTRUUUUUUUUUUUEEEEEEEE"<<std::endl;
	}

	if (params.int8)
	{
		config->setFlag(BuilderFlag::kINT8);
		trtCommon::setAllDynamicRanges(network.get(), 127.0f, 127.0f);
	}

	trtCommon::enableDLA(builder.get(), config.get(), params.dlaCore);

	if (trtCommon::getSMVersion() >= 0x500)
	{
		auto tactics = static_cast<uint32_t>(config->getTacticSources());
		tactics = tactics & ~(1U << static_cast<uint32_t>(TacticSource::kCUDNN));
		config->setTacticSources(static_cast<TacticSources>(tactics));
	}

	return true;
}


bool Onnx2Trt(const BuildParams& params, std::shared_ptr<ICudaEngine>& engine, std::shared_ptr<IExecutionContext>& context)
{
	auto builder = TrtUniquePtr<nvinfer1::IBuilder>(nvinfer1::createInferBuilder(gLogger.getTRTLogger()));
	if (!builder)
	{
		// PLOGE << "Create Builder Failure.";
		return false;
	}

	const auto explicitBatch = 1U << static_cast<uint32_t>(NetworkDefinitionCreationFlag::kEXPLICIT_BATCH);
	auto network = TrtUniquePtr<nvinfer1::INetworkDefinition>(builder->createNetworkV2(explicitBatch));
	if (!network)
	{
		// PLOGE << "Create network Failure.";
		return false;
	}

	auto config = TrtUniquePtr<nvinfer1::IBuilderConfig>(builder->createBuilderConfig());
	if (!config)
	{
		// PLOGE << "Create config Failure.";
		return false;
	}

	auto parser = TrtUniquePtr<nvonnxparser::IParser>(nvonnxparser::createParser(*network, gLogger.getTRTLogger()));
	if (!parser)
	{
		// PLOGE << "Create parser Failure.";
		return false;
	}

	auto constructed = ConstructNetwork(params, builder, network, config, parser);
	if (!constructed)
	{
		// PLOGE << "ConstructNetwork Failure.";
		return false;
	}

	builder->setMaxBatchSize(1);

	// CUDA stream used for profiling by the builder.
	auto profileStream = trtCommon::makeCudaStream();
	if (!profileStream)
	{
		// PLOGE << "makeCudaStream Failure.";
		return false;
	}
	config->setProfileStream(*profileStream);

	TrtUniquePtr<IHostMemory> plan{ builder->buildSerializedNetwork(*network, *config) };
	if (!plan)
	{
		// PLOGE << "buildSerializedNetwork Failure.";
		return false;
	}

	std::ofstream ofs(params.trtFileName, std::ios::out | std::ios::binary);
	ofs.write((char*)(plan->data()), plan->size());
	ofs.close();

	// PLOGI << "Build engine Succes.";

	TrtUniquePtr<IRuntime> runtime{ createInferRuntime(gLogger.getTRTLogger()) };
	if (!runtime)
	{
		// PLOGE << "createInferRuntime Failure.";
		return false;
	}

	engine = std::shared_ptr<ICudaEngine>(runtime->deserializeCudaEngine(plan->data(), plan->size()), trtCommon::InferDeleter());
	if (!engine)
	{
		// PLOGE << "deserializeCudaEngine Failure.";
		return false;
	}

	context = TrtUniquePtr<nvinfer1::IExecutionContext>(engine->createExecutionContext());
	if (!context)
	{
		// PLOGE << "createExecutionContext Failure.";
		return false;
	}

	return true;
}

bool LoadTrtEngine(const std::string trtFileName, std::shared_ptr<ICudaEngine>& engine, std::shared_ptr<IExecutionContext>& context)
{
	if (!isExistFile(trtFileName))
	{
		// PLOGE << "trtFileName does not exists:" << trtFileName;
		return false;
	}

	std::ifstream t(trtFileName, std::ios::in | std::ios::binary);
	std::stringstream tempStream;
	tempStream << t.rdbuf();
	t.close();

	tempStream.seekg(0, std::ios::end);
	const int modelSize = (int)tempStream.tellg();
	tempStream.seekg(0, std::ios::beg);
	void* modelMem = malloc(modelSize);
	tempStream.read((char*)modelMem, modelSize);

	initLibNvInferPlugins(&gLogger.getTRTLogger(), "");

	TrtUniquePtr<IRuntime> runtime{ createInferRuntime(gLogger.getTRTLogger()) };
	if (!runtime)
	{
		// PLOGE << "Build Runtime Failure.";
		return false;
	}

	engine = std::shared_ptr<ICudaEngine>(runtime->deserializeCudaEngine(modelMem, modelSize), trtCommon::InferDeleter());
	if (!engine)
	{
		// PLOGE << "Build Engine Failure.";
		return false;
	}

	context = TrtUniquePtr<nvinfer1::IExecutionContext>(engine->createExecutionContext());
	if (!context)
	{
		// PLOGE << "Build context Failure.";
		return false;
	}

	return true;
}