
#include "TrtInfer.hpp"
#include "Utils.hpp"
#include "TrtCommon.h"

using namespace std;

TrtInfer::TrtInfer()
{
}

TrtInfer::~TrtInfer()
{
	Uninit();
}

bool TrtInfer::PostProcess(std::vector<float>& outputData)
{
	// get the predStuff's shape and convert vector<int>
	vector<int> outShape;
	outShape.assign(m_predShape.begin(), m_predShape.end());

    // get output: select the foreground class
    std::vector<float> out(m_predData + m_outputDataSize[4] / 2, m_predData + m_outputDataSize[4]);
	// std::cout << m_outputDataSize[4]<< std::endl; // m_outputDataSize[4] = 1x2x128x128x128
	outputData.assign(out.begin(), out.end());

	return true;
}

void TrtInfer::GetInputInfos()
{
	this->m_inputNames.clear();
	this->m_inputSize.clear();
	this->m_inputShape.clear();
	this->m_inputDataSize.clear();
	this->m_inputHostDataBuffer.clear();
	this->m_inputDeviceDataBuffer.clear();

	int numInputs = 0;
	for (int32_t i = 0; i < m_engine->getNbBindings(); i++)
	{
		if (m_engine->bindingIsInput(i))
		{
			numInputs = numInputs + 1;
		}
	}

	this->m_inputData.resize(numInputs);

	for (int32_t i = 0; i < m_engine->getNbBindings(); i++)
	{
		if (!m_engine->bindingIsInput(i))
		{
			continue;
		}

		string inName = m_engine->getBindingName(i);
		this->m_inputNames.push_back(inName);
		int32_t srcIndex = m_engine->getBindingIndex(inName.c_str());
		Dims srcDims = m_context->getBindingDimensions(srcIndex);

		this->m_inputSize.push_back(m_buffers->size(inName));
		this->m_inputDataSize.push_back(m_buffers->size(inName) / sizeof(float));
		this->m_inputHostDataBuffer.push_back(static_cast<float*>(m_buffers->getHostBuffer(inName)));
		this->m_inputDeviceDataBuffer.push_back(static_cast<float*>(m_buffers->getDeviceBuffer(inName)));

		vector<int64_t> tmpShape;
		for (size_t j = 0; j < srcDims.nbDims; j++)
		{
			tmpShape.push_back(srcDims.d[j]);
		}

		this->m_inputShape.push_back(tmpShape);
		this->m_inputData[i].resize(m_inputDataSize[i]);

		string printStr = ", input shape: {";
		for (size_t j = 0; j < tmpShape.size(); j++)
		{
			if (j != tmpShape.size() - 1)
			{
				printStr += to_string(tmpShape[j]) + ", ";
			}
			else
			{
				printStr += to_string(tmpShape[j]);
			}

		}
		printStr += "}";
		std::cout << "input name: " << inName << printStr << std::endl;
	}
}

void TrtInfer::GetOutputInfos()
{
	this->m_outputNames.clear();
	this->m_outputSize.clear();
	this->m_outputShape.clear();
	this->m_outputDataSize.clear();
	this->m_outputHostDataBuffer.clear();
	this->m_outputDeviceDataBuffer.clear();

	for (int32_t i = 0; i < m_engine->getNbBindings(); i++)
	{
		if (m_engine->bindingIsInput(i))
		{
			continue;
		}

		string outName = m_engine->getBindingName(i);
		this->m_outputNames.push_back(outName);

		int32_t srcIndex = m_engine->getBindingIndex(outName.c_str());
		Dims srcDims = m_context->getBindingDimensions(srcIndex);

		vector<int64_t> tmpShape;
		for (size_t j = 0; j < srcDims.nbDims; j++)
		{
			tmpShape.push_back(srcDims.d[j]);
		}

		this->m_outputShape.push_back(tmpShape);
		this->m_outputSize.push_back(m_buffers->size(outName));
		this->m_outputDataSize.push_back(m_buffers->size(outName) / sizeof(float));
		this->m_outputHostDataBuffer.push_back(static_cast<float*>(m_buffers->getHostBuffer(outName)));
		this->m_outputDeviceDataBuffer.push_back(static_cast<float*>(m_buffers->getDeviceBuffer(outName)));

		string printStr = ", output shape: {";
		for (size_t j = 0; j < tmpShape.size(); j++)
		{
			if (j != tmpShape.size() - 1)
			{
				printStr += to_string(tmpShape[j]) + ", ";
			}
			else
			{
				printStr += to_string(tmpShape[j]);
			}

		}
		printStr += "}";
		std::cout << "output name: " << outName << printStr << std::endl;
	}
}

bool TrtInfer::Init(const InitParam& initParam)
{
	// --------------------------------- Step 1. param convert ----------------------------------------
	BuildParams param;
	param.onnxFile = initParam.modelPath;
	param.trtFileName = contactPath(initParam.trtModelPath, m_engineName);

	// --------------------------- Step 2. read engine or convert onnx ----------------------------------
	// if trtFileName is not existed, it will build
	if (!isExistFile(param.trtFileName))
	{
		if (!isExistDirectory(initParam.trtModelPath))
		{
			createDiretory(initParam.trtModelPath);
		}

		if (!isExistFile(param.onnxFile))
		{
			std::cout << "ModelPath does not exists: " << param.onnxFile << std::endl;
			return false;
		}

		if (!Onnx2Trt(param, m_engine, m_context))
		{
			std::cout << "Onnx2Trt failed." << std::endl;
			return false;
		}
	}
	else
	{
		if (!LoadTrtEngine(param.trtFileName, m_engine, m_context))
		{
			std::cout << "LoadTrtEngine failed."<< std::endl;
			return false;
		}
	}

	// -------------------------------- Step 4. make buffer ---------------------------------------
	m_maxBatchSize = m_engine->getMaxBatchSize();
	// mallloc input and output buffer
	m_buffers = new BufferManager(m_engine, m_maxBatchSize);

	std::cout << "m_maxBatchSize: " << m_maxBatchSize<< std::endl;

	// --------------------------------    Step 5. get input info     ----------------------------------------
	GetInputInfos();

	// --------------------------------    Step 6. get output info     ----------------------------------------
	GetOutputInfos();

	// -------------    Step 7. Create CUDA stream for the execution of this inference.     --------------------
	CHECK(cudaStreamCreate(&m_stream));

	return true;
}


void TrtInfer::Uninit()
{
}

bool TrtInfer::TrtProcess(std::vector<float>& inpuData)
{
	int status = 0;

	// 1.Memcpy data to host
	for (size_t i = 0; i < m_inputDeviceDataBuffer.size(); i++)
	{
		size_t byteSize = this->m_inputSize[i];
		float* hostData = (float*)inpuData.data();
		float* deviceData = this->m_inputDeviceDataBuffer[i];
		status = cudaMemcpyAsync(deviceData, hostData, byteSize, cudaMemcpyHostToDevice, m_stream);

		if (status != 0)
		{
			std::cout << "cudaMemcpyAsync failed: " << status<< std::endl;
			return false;
		}
	}
	//cudaStreamSynchronize(m_stream);

	// 2. infer
	bool inferStatus = m_context->enqueueV2(m_buffers->getDeviceBindings().data(), m_stream, nullptr);
	if (!inferStatus)
	{
		std::cout << "executeV2 failed."<< std::endl;
		return false;
	}
	//cudaStreamSynchronize(m_stream);

	//3.get output result
	for (size_t i = 0; i < m_outputHostDataBuffer.size(); i++)
	{
		size_t byteSize = this->m_outputSize[i];
		float* hostData = this->m_outputHostDataBuffer[i];
		float* deviceData = this->m_outputDeviceDataBuffer[i];
		status = cudaMemcpyAsync(hostData, deviceData, byteSize, cudaMemcpyDeviceToHost, m_stream);

		if (status != 0)
		{
			std::cout << "cudaMemcpyAsync failed: " << status<< std::endl;
			return false;
		}
	}

	// Wait for the work in the stream to complete.
	status = cudaStreamSynchronize(m_stream);
	if (status != 0)
	{
		std::cout << "cudaStreamSynchronize failed: " << status<< std::endl;
		return false;
	}

	return true;
}

bool TrtInfer::Process(std::vector<float>& inputData, std::vector<float>& outputData)
{
	// 1. condition
	if (inputData.size() != this->m_inputDataSize[0])
	{
		std::cout << "inpuData size error."<< std::endl;
		return false;
	}

	chrono::high_resolution_clock::time_point _start = chrono::high_resolution_clock::now();

	// 2. trt inference
	if (!TrtProcess(inputData))
	{
		std::cout << "TrtProcess failed."<< std::endl;
		return false;
	}

	chrono::duration<double, milli> _dur = (chrono::high_resolution_clock::now() - _start);
	printf("%f ms\n", _dur.count());

	// 3. get trt results
	m_predData = m_outputHostDataBuffer[4];
	m_predShape = m_outputShape[4];

	// 4. PostProcess
	if (!this->PostProcess(outputData))
	{
		std::cout << "PostProcess failed."<< std::endl;
		return false;
	}

	return true;
}

