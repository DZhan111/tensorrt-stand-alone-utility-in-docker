#pragma once

#include <string>

static bool is_str_utf8(const char* str);
static std::wstring UTF8ToUnicode(const std::string& str);

size_t readBinaryFile(const char* filename, unsigned char*& data);
size_t writeBinaryFile(const char* filename, const unsigned char* data, size_t size);
bool isExistFile(const std::string& path);

std::string contactPath(const std::string& path1, const std::string& path2);

bool isExistDirectory(const std::string& directory);
bool createDiretory(const std::string& directory);