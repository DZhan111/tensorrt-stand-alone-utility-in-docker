target_include_directories(${lib_name} PRIVATE 
							$<BUILD_INTERFACE: common>)


target_include_directories(${lib_name} PRIVATE 
							$<BUILD_INTERFACE: common/tensorrt> 
							$<BUILD_INTERFACE: tensorrt>)
							# $<BUILD_INTERFACE: ${CUDA_INCLUDE}>) 
							# $<BUILD_INTERFACE: ${TRT_INCLUDE}>)

target_link_libraries(${lib_name} ${TRT_LIB}) # ${CUDA_LIB})