set(Common_SOURCE_LIST
	common/Utils.hpp
	common/Utils.cpp
	common/tensorrt/half.h
	common/tensorrt/logger.h
	common/tensorrt/logger.cpp
	common/tensorrt/logging.h
	common/tensorrt/buffers.h
	common/tensorrt/common.h
	common/tensorrt/ErrorRecorder.h
	common/tensorrt/TrtCommon.h
	common/tensorrt/TrtCommon.cpp
	common/tensorrt/Device.h)
source_group(TREE ${CMAKE_CURRENT_SOURCE_DIR}/../ FILES ${Common_SOURCE_LIST})

set(SOURCE_LIST
	AIEngine.hpp
	AIEngine.cpp
	TrtInfer.cpp
	TrtInfer.hpp)

source_group(TREE ${CMAKE_CURRENT_SOURCE_DIR}/../ FILES ${SOURCE_LIST})