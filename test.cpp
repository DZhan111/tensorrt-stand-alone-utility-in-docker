#include "AIEngine.hpp"
#include <vtkImageCast.h>
#include <vtkNIFTIImageReader.h>
#include <vtkImageViewer2.h>
#include <vtkNamedColors.h>
#include <vtkNew.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkRenderer.h>
#include <vtkSmartPointer.h>
#include <vtkImageActor.h>
#include <vtkImageData.h>
#include <vtkCamera.h>
#include <vtkPointData.h>
#include <vtkDataArray.h>
#include <vtkAbstractArray.h>
#include <vtkNIFTIImageWriter.h>
#include <vtkInformation.h>
#include <vtkImageThreshold.h>
#include <vtkImageMathematics.h>
#include <vtkImageShiftScale.h>
#include <vtkImageResize.h>
#include <vtkImageSincInterpolator.h>
#include "vtkAutoInit.h" 
VTK_MODULE_INIT(vtkRenderingOpenGL2);
VTK_MODULE_INIT(vtkInteractionStyle);

using namespace std;

static void Help()
{
	printf("\nExamples of the AIEngine algorithm\n"
		"Usage: \n"
		"          ./AIEngine <Path to the model file> <trt model save path> <image path>\n\n"
		"Press:\n"
		"Press Esc to exit from video \n\n"
	);
}


void VTKReadImage(const std::string& imgPath, vtkSmartPointer<vtkImageData>& imageData)
{
    // Read all the NIFTI file.
    vtkNew<vtkNIFTIImageReader> nifti_reader;
    vtkSmartPointer<vtkImageCast> imageCast = vtkSmartPointer<vtkImageCast>::New();
    // Set true for 4D data
    // nifti_reader->SetTimeAsVector(true);
    nifti_reader->SetFileName(imgPath.c_str());
    nifti_reader->Update();

    imageCast->SetInputConnection(nifti_reader->GetOutputPort());
    imageCast->SetOutputScalarTypeToInt();
    imageCast->Update();

    // Get the output data
    imageData->DeepCopy(nifti_reader->GetOutput());
}

void VTKPreProcess(const vtkSmartPointer<vtkImageData>& imageData, std::vector<float>& outData)
{
    // Resample to 128x128x128
    // Create a resampler and set the input image
    vtkSmartPointer<vtkImageResize> resampler = vtkSmartPointer<vtkImageResize>::New();
    resampler->SetInputData(imageData);

    // Set the dimensions of the output image to 128*128*128
    resampler->SetOutputDimensions(128,128,128);

    // Set the interpolation mode to linear
    vtkNew<vtkImageSincInterpolator> interpolator;
    interpolator->UseWindowParameterOn();
    resampler->SetInterpolator(interpolator);

    // Update the resampler
    resampler->Update();
    
    imageData ->DeepCopy(resampler->GetOutput());

    // Get the point data
    vtkSmartPointer<vtkPointData> pointData = imageData->GetPointData();

    // Get the CT values
    // imageData->Print(std::cout);
    vtkAbstractArray* array = pointData->GetAbstractArray(0);
    const char* name = array->GetName();
    // std::cout << "Name of array 0: " << name << std::endl;
    vtkSmartPointer<vtkDataArray> ctValues = pointData->GetArray(name); 

    // Access the pixel values
    int* dimensions = imageData->GetDimensions();
    //short* pixelData = static_cast<short*>(imageData->GetScalarPointer());
    for (int z = 0; z < dimensions[2]; z++)
    {
        for (int y = 0; y < dimensions[1]; y++)
        {
            for (int x = 0; x < dimensions[0]; x++)
            {
                int index = (z * dimensions[1] + y) * dimensions[0] + x;
                //short pixelValue = pixelData[index];
                // std::cout<< pixelValue <<endl;
                double ctValue = ctValues->GetComponent(index, 0);
                //std::cout<< pixelValue << "," << ctValue <<endl;
                
                // Normalization
                if (ctValue > 259) {ctValue = 259;}
                if (ctValue < -856) {ctValue = -856;}
                ctValue -= -119.19; 
                ctValue /= 239.11; 
                //std::cout << ctValue << std::endl;
                outData[index] = (float)ctValue;
            }
        }
    }
}

void VTKSoftMax(vtkSmartPointer<vtkImageData>& imageData)
{
    // e^x Create a math filter for exponentiation
    vtkSmartPointer<vtkImageMathematics> expFilter = vtkSmartPointer<vtkImageMathematics>::New();

    // Set the input image data
    expFilter->SetInputData(imageData);

    // Set the operation to exponentiation
    expFilter->SetOperationToExp();

    // Apply the exponentiation e^x
    expFilter->Update();

    // 1+e^x Create a vtkImageShiftScale object
    vtkSmartPointer<vtkImageShiftScale> addFilter =
        vtkSmartPointer<vtkImageShiftScale>::New();
    addFilter->SetInputData(expFilter->GetOutput());
    addFilter->SetShift(1.0);
    addFilter->SetScale(1.0);

    // Update the filter
    addFilter->Update();

    //  e^x/(1+e^x) Create a math filter for division
    vtkSmartPointer<vtkImageMathematics> divFilter = vtkSmartPointer<vtkImageMathematics>::New();

    // Set the input image data
    divFilter->SetInput1Data(expFilter->GetOutput());

    // Set the input image data
    divFilter->SetInput2Data(addFilter->GetOutput());

    // Set the operation to division
    divFilter->SetOperationToDivide();

    // Apply the division
    divFilter->Update();

    imageData = divFilter->GetOutput();

    // Set Softmax threshold as 0.5
    vtkSmartPointer<vtkImageThreshold> threshold = vtkSmartPointer<vtkImageThreshold>::New();

    // Set the input image data
    threshold->SetInputData(imageData);

    // Set the threshold range
    threshold->ThresholdByLower(0.5);

    // Set the threshold values
    threshold->SetInValue(0.0);
    threshold->SetOutValue(1.0);

    // Apply the thresholding
    threshold->Update();

    // Get the output image data
    imageData = threshold->GetOutput();
}

void VTKConnectivityCheck(const vtkSmartPointer<vtkImageData>& imageData)
{
    // Remove all other nodules and keep only the clicked one

    int extent[6];
    imageData->GetExtent(extent);
    double spacing[3];
    imageData->GetSpacing(spacing);

    vtkSmartPointer<vtkPoints> seed = vtkSmartPointer<vtkPoints>::New();
    seed->InsertNextPoint((extent[0]+extent[1])/2 * spacing[0], (extent[2]+extent[3])/2 * spacing[1], (extent[4]+extent[5])/2 * spacing[2]);
    vtkSmartPointer<vtkPolyData> seedData = vtkSmartPointer<vtkPolyData>::New();
    seedData->SetPoints(seed);
    vtkSmartPointer<vtkImageConnectivityFilter> connectivity_filter = vtkSmartPointer<vtkImageConnectivityFilter>::New();
    connectivity_filter->SetInputData(imageData);
    connectivity_filter->SetSeedData(seedData);
    connectivity_filter->SetExtractionModeToSeededRegions();
    // connectivity_filter->SetExtractionModeToLargestRegion();
    connectivity_filter->Update();

    imageData->DeepCopy(connectivity_filter->GetOutput());

}

void VTKShowImage(const vtkSmartPointer<vtkImageData>& imageData)
{
    // Visualize
    vtkNew<vtkImageViewer2> imageViewer;
    vtkNew<vtkRenderWindowInteractor> renderWindowInteractor;
    imageViewer->GetRenderWindow()->SetSize(512, 512);
    imageViewer->GetRenderWindow()->SetWindowName("ReadNIFTI");

    vtkNew<vtkNamedColors> colors;
    imageViewer->GetRenderer()->SetBackground(
            colors->GetColor3d("DarkSlateGray").GetData());

    // Axial/Sagittal/Coronal View
    imageViewer->SetSliceOrientationToXZ();
    // Adjust Window/Level
    double range[2];
    imageData->GetScalarRange(range);
    imageViewer->SetColorLevel(range[1]);
    imageViewer->SetColorWindow(range[1] * 2);

    imageViewer->SetInputData(imageData);
    // Set to middle slice
    imageViewer->SetSlice((imageViewer->GetSliceMax() + imageViewer->GetSliceMin()) / 2);
    imageViewer->SetupInteractor(renderWindowInteractor);

    // Render image
    imageViewer->Render();
    renderWindowInteractor->Start();
}

void VTKSaveImage(const vtkSmartPointer<vtkImageData>& imageData, const std::string& saveName)
{
     // Create a NIFTI image writer
    vtkSmartPointer<vtkNIFTIImageWriter> writer = vtkSmartPointer<vtkNIFTIImageWriter>::New();

    // Set the input image data
    writer->SetInputData(imageData);

    // Set the output file name
    writer->SetFileName(saveName.c_str());

    // Write the image data
    writer->Update();
}

int main(int argc, char* argv[])
{
	// -------- Step 1. Help and CommandLineParser --------
    if (argc != 4)
    {
       Help();
       return -1;
    }
    

	// -------- Step 2. Make Engine --------
	InitParam initParam;
	initParam.modelPath = argv[1];
	initParam.trtModelPath = argv[2];

	std::shared_ptr<AIEngine> detEngine = make_shared<AIEngine>();

	if (!detEngine->Init(initParam))
	{
		std::cout << "Init failed.";
		return -1;
	}

	// -------- Step 3. read image --------
	string imgPath = argv[3];
	vtkSmartPointer<vtkImageData> imageData = vtkSmartPointer<vtkImageData>::New();
    VTKReadImage(imgPath, imageData);

    // -------- Step 4. pre process --------
    std::vector<float> inputData;
    inputData.resize(128*128*128);
    VTKPreProcess(imageData, inputData);

	// -------- Step 5. process --------
    std::vector<float> outputData;
    detEngine->Process(inputData, outputData);
    //std::cout<< outputData.size() <<std::endl;


    // -------- Step 6. output to vtk --------
    vtkSmartPointer<vtkImageData> imgOut = vtkSmartPointer<vtkImageData>::New();
    vtkSmartPointer<vtkInformation> info = vtkSmartPointer<vtkInformation>::New(); 
    imgOut->SetDimensions(128,128,128);
    imgOut->SetScalarType(VTK_FLOAT,info);
	imgOut->SetNumberOfScalarComponents(1,info);// Set the number of scalar components in the image to 1 (Grey:1, RGB:3)
	imgOut->AllocateScalars(info);// Allocate memory for the image data
    float* imgOutData = static_cast<float*>(imgOut->GetScalarPointer());
    for (int z = 0; z < 128; z++)
    {
        for (int y = 0; y < 128; y++)
        {
            for (int x = 0; x < 128; x++)
            {
                int index = (z * 128 + y) * 128 + x;
                imgOutData[index] = outputData[index];
            }
        }
    }
    
    // SoftMax Layer
    VTKSoftMax(imgOut);

    // Remove all other nodules and keep only the clicked one
    VTKConnectivityCheck(imgOut);
    
    //show
    // VTKShowImage(imgOut);

    //save
    VTKSaveImage(imgOut, "output.nii.gz");

	return 0;
}
