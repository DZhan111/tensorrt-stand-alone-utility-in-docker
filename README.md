# TensorRT stand-alone utility in docker

This repo includes the initial TensorRT stand-alone utility that runs in docker.

## Configuration
To get started, the official docker (container version 23.01) of TensorRT 8.5.2 can be download [here](https://docs.nvidia.com/deeplearning/tensorrt/container-release-notes/#rel-23-01). Follow the instructions on the website to launch the docker with your local directory linked to the docker. Below is an example to use the command to launch the docker where `--runtime=nvidia` is added to use GPU.

`sudo docker run -it --rm --runtime=nvidia -v /home/dzhan111@na.jnj.com/Nodule_Segmentation/tensorrt/docker:/workspace/tensorrt/docker nvcr.io/nvidia/tensorrt:21.07-py3`

Then, download the VTK 8.2.0 [source code](https://vtk.org/download/) and compile it:

`apt-get update`

`apt-get install libgl1-mesa-dev`

`apt-get install libxt-dev`

If you prefer ccmake:
`apt-get install cmake-curses-gui`

`cd build`

`ccmake /path/to/VTK/sourcecode`

`make -j 8`

`make install`

`ldconfig`


## Run the TensorRT stand-alone utility
Clone this repo to local, add the directory to the docker and launch it. Compile the codes using the below steps.

`cd build`

`ccmake /path/to/cpp`

`make`

`./xxxxxx`

## Reference

1. https://aurisrobotics.atlassian.net/wiki/spaces/EG/pages/3234169192/
